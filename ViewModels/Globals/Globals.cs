﻿using System;
using System.Collections.Generic;
using AzureClient.Model;
using CommsDb.Database;
using Interfaces.Interfaces;
using Service.Interfaces;
using Utils;
using Xamarin.Forms;

namespace ViewModels
{
    public static class Globals
    {
        public const string IDS_DATABASE = "IdsDatabase";
#if DEBUG
        public const int GPS_MIN_DISTANCE_IN_METERS = 5;
#else
        public const int GPS_MIN_DISTANCE_IN_METERS = 50;
#endif

        public const int MAX_STATIONARY_MINUTES = 2;

        public static bool OnLine;
        public static readonly List<Action<bool>> OnPingChange = new List<Action<bool>>();
        public static MainPageViewModel MainPageViewModel;
        public static IService Service = new AzureRemoteService.AzureClient();
        public static Db<Trip> Database = new Db<Trip>( Service, IDS_DATABASE );

        private static IGpsProvider _GpsService;
        public static IGpsProvider GpsService => _GpsService ?? ( _GpsService = DependencyService.Get<IGpsProvider>() );

        private static Distance.DateTimePoint LastPoint = new Distance.DateTimePoint();

        static Globals()
        {
            Service.OnPing = status =>
            {
                var OLine = status == PING_STATUS.ONLINE;
                OnLine = OLine;
                foreach( var Action in OnPingChange )
                    Action?.Invoke( OLine );
            };

            GpsService.OnNewPosition = point =>
            {
                if( MenuButtonVisible ) // Need to be signed in
                {
                    var NextPoint = new Distance.DateTimePoint { Longitude = point.Longitude, Latitude = point.Latitude, DateTime = point.Time };
                    if( LastPoint.IsZero || Distance.IsDistanceOk( LastPoint, NextPoint, GPS_MIN_DISTANCE_IN_METERS, MAX_STATIONARY_MINUTES ) )
                    {
                        Database.AddGpsPoint( point.Time, point.Latitude, point.Longitude );
                        LastPoint = NextPoint;
                    }
                }
            };
        }

        public static bool MenuButtonVisible
        {
            get => MainPageViewModel.MenuButtonVisible;
            set => MainPageViewModel.MenuButtonVisible = value;
        }

        public static string GetStringSetting( string settingId )
        {
            try
            {
                return (string)Application.Current.Properties[ settingId ];
            }
            catch
            {
                return "";
            }
        }

        public static async void SaveStringSetting( string settingId, string value )
        {
            try
            {
                Application.Current.Properties[ settingId ] = value;
                await Application.Current.SavePropertiesAsync();
            }
            catch
            {
            }
        }

        public static bool GetBoolSetting( string settingId )
        {
            try
            {
                return (bool)Application.Current.Properties[ settingId ];
            }
            catch
            {
                return false;
            }
        }

        public static async void SaveBoolSetting( string settingId, bool value )
        {
            try
            {
                Application.Current.Properties[ settingId ] = value;
                await Application.Current.SavePropertiesAsync();
            }
            catch
            {
            }
        }

        public static int GetIntSetting( string settingId )
        {
            try
            {
                return (int)Application.Current.Properties[ settingId ];
            }
            catch
            {
                return -1;
            }
        }

        public static async void SaveIntSetting( string settingId, int value )
        {
            try
            {
                Application.Current.Properties[ settingId ] = value;
                await Application.Current.SavePropertiesAsync();
            }
            catch
            {
            }
        }
    }
}