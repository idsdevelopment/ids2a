﻿using System;
using System.Windows.Input;
using Models.ViewModelSupport;

namespace ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        public const string MANUAL_BARCODE_INPUT = "ManualBarcodeInput";
        public const string VISIBILITY = "SortVisibility";
        public const string SORT1 = "Sort1";
        public const string SORT2 = "Sort2";

        public enum SORT_VISIBILITY
        {
            ALL,
            PUCK_UP,
            DELIVERY,
            NEW
        }

        public enum SORT_OPTIONS
        {
            ALL,
            ID,
            DUE,
            ACCOUNT_ID,
            ACCOUNT_NUMBER,
            COMPANY_NAME,
            REFERENCE,
            NEW
        }

        private readonly bool Initialised;

        public MainPageViewModel()
        {
            Globals.MainPageViewModel = this;
            ManualBarcodeInput = Globals.GetBoolSetting( MANUAL_BARCODE_INPUT );
            VisibilityIndex = Globals.GetIntSetting( VISIBILITY );
            Sort1Index = Globals.GetIntSetting( SORT1 );
            Sort2Index = Globals.GetIntSetting( SORT2 );
            Globals.OnPingChange.Add( OnPingChange );
            Initialised = true;
        }

        private void OnPingChange( bool online )
        {
            Dispatcher( () =>
            {
                OnLine = online;
                OffLine = !online;
            } );
        }

        ~MainPageViewModel()
        {
            Globals.OnPingChange.Remove( OnPingChange );
        }

        public Action<bool> OnShowMenu;
        public Action OnSignOut;
        public Action<string> OnScannedBarcode;
        public Action<SORT_VISIBILITY> OnSortVisibilityChange;
        public Action<SORT_OPTIONS> OnSort1Change;
        public Action<SORT_OPTIONS> OnSort2Change;

        private bool InMenuShow;
        private bool _MenuVisible;

        public void ClearContent()
        {
            Dispatcher( () =>
            {
                
            } );
        }

        public bool MenuVisible
        {
            get => _MenuVisible;
            set
            {
                _MenuVisible = !value;      // Shwow Menu re-inverts it assuming it's a button toggle
                Execute_ShowMenu();
            }
        }

        public void Execute_ShowMenu()
        {
            if( !InMenuShow )
            {
                InMenuShow = true;
                try
                {
                    _MenuVisible = !_MenuVisible;
                    if( OnShowMenu != null )
                        Dispatcher( () => { OnShowMenu( _MenuVisible ); } );
                }
                catch
                {
                }
                finally
                {
                    InMenuShow = false;
                }
            }
        }

        public ICommand ShowMenu => Commands[ nameof( ShowMenu ) ];

        public void Execute_SignOut()
        {
            OnSignOut?.Invoke();
            Execute_ShowMenu();
            MenuButtonVisible = false;
            BarcodeScannerVisible = false;
        }

        public ICommand SignOut => Commands[ nameof( SignOut ) ];

        public string MainTitle
        {
            get { return Get( () => MainTitle ); }
            set { Set( () => MainTitle, value ); }
        }



        public bool OnLine
        {
            get { return Get( () => OnLine ); }
            set { Set( () => OnLine, value ); }
        }

        public bool OffLine
        {
            get { return Get( () => OffLine, true ); }
            set { Set( () => OffLine, value ); }
        }

        public bool MenuButtonVisible
        {
            get { return Get( () => MenuButtonVisible ); }
            set { Set( () => MenuButtonVisible, value ); }
        }




        public bool BarcodeScannerVisible
        {
            get { return Get( () => BarcodeScannerVisible ); }
            set { Set( () => BarcodeScannerVisible, value ); }
        }

        public bool ManualBarcodeInput
        {
            get { return Get( () => ManualBarcodeInput ); }
            set { Set( () => ManualBarcodeInput, value ); }
        }

        [ DependsUpon( nameof( ManualBarcodeInput ) ) ]
        public void WhenNamualBarcodeInputChanges()
        {
            Globals.SaveBoolSetting( MANUAL_BARCODE_INPUT, ManualBarcodeInput );
        }

        public string BarcodeInput
        {
            get { return Get( () => BarcodeInput ); }
            set { Set( () => BarcodeInput, value ); }
        }

        [ DependsUpon( nameof( BarcodeInput ), Delay = 300 ) ]
        public void WhenBarcodeInputChanges()
        {
            OnScannedBarcode?.Invoke( BarcodeInput );
        }

        public int VisibilityIndex
        {
            get { return Get( () => VisibilityIndex ); }
            set { Set( () => VisibilityIndex, value ); }
        }

        [ DependsUpon( nameof( VisibilityIndex ) ) ]
        public void WhenVisibilityIndexChanges()
        {
            if( Initialised )
            {
                Globals.SaveIntSetting( VISIBILITY, VisibilityIndex );
                OnSortVisibilityChange?.Invoke( (SORT_VISIBILITY)VisibilityIndex );
            }
        }

        public int Sort1Index
        {
            get { return Get( () => Sort1Index ); }
            set { Set( () => Sort1Index, value ); }
        }

        [ DependsUpon( nameof( Sort1Index ) ) ]
        public void WhenSort1IndexIndexChanges()
        {
            if( Initialised )
            {
                Globals.SaveIntSetting( SORT1, Sort1Index );
                OnSort1Change?.Invoke( (SORT_OPTIONS)Sort1Index );
            }
        }

        public int Sort2Index
        {
            get { return Get( () => Sort2Index ); }
            set { Set( () => Sort2Index, value ); }
        }

        [ DependsUpon( nameof( Sort2Index ) ) ]
        public void WhenSort2IndexIndexChanges()
        {
            if( Initialised )
            {
                Globals.SaveIntSetting( SORT2, Sort2Index );
                OnSort1Change?.Invoke( (SORT_OPTIONS)Sort2Index );
            }
        }
    }
}