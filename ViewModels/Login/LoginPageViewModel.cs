﻿using System;
using System.Windows.Input;
using Models.ViewModelSupport;
using Service.Interfaces;
using Utils;
using Xamarin.Forms;

namespace ViewModels.Login
{
    public class LoginPageViewModel : ViewModelBase
    {
        public delegate void OnSignInEvent( bool oK );

        public OnSignInEvent OnSignIn;
        public Action OnTimeError;
        public Func<string> OnGetBarcode;

        private const string ACCOUNT = "Account",
                             USER_NAME = "UserName",
                             PASSWORD = "Password",
                             TESTING_SERVER = "TestingServer",
                             DRAGONS = "There Be Dragons Here";

        private void OnPingChange( bool onLine )
        {
            EnableSignIn();
        }

        public LoginPageViewModel()
        {
            Account = Globals.GetStringSetting( ACCOUNT );
            UserName = Globals.GetStringSetting( USER_NAME );
            Password = Globals.GetStringSetting( PASSWORD );
            TestingServer = Globals.GetBoolSetting( TESTING_SERVER );
            Globals.OnPingChange.Add( OnPingChange );
        }

        ~LoginPageViewModel()
        {
            Globals.OnPingChange.Remove( OnPingChange );
        }

        private static bool IsDragon( string txt )
        {
            return txt.Trim().Equals( DRAGONS, StringComparison.InvariantCultureIgnoreCase );
        }

        public async void Execute_Login()
        {
            if( IsDragon( Account ) || IsDragon( UserName ) || IsDragon( Password ) )
            {
                TestingServer = true;
                Globals.SaveBoolSetting( TESTING_SERVER, true );
                Account = "";
                UserName = "";
                Password = "";
            }
            else
            {
                SignInEnabled = false;
                try
                {
                    switch( await Globals.Service.Connect( Account, UserName, Password ) )
                    {
                    default:
                        OnSignIn?.Invoke( false );
                        break;
                    case CONNECTION_STATUS.OK:
                        Globals.SaveStringSetting( ACCOUNT, Account );
                        Globals.SaveStringSetting( USER_NAME, UserName );
                        Globals.MenuButtonVisible = true;
                        OnSignIn?.Invoke( true );
                        break;
                    case CONNECTION_STATUS.TIME_ERROR:
                        OnTimeError?.Invoke();
                        break;
                    }
                }
                finally
                {
                    SignInEnabled = true;
                }
            }
        }

        public ICommand Login
        {
            get
            {
                var Temp = Commands[ nameof( Login ) ];
                return Temp;
            }
        }

        public void Execute_GetBarcode()
        {
            if( OnGetBarcode != null )
                Dispatcher( () => { Password = OnGetBarcode(); } );
        }

        public ICommand GetBarcode
        {
            get
            {
                var Temp = Commands[ nameof( GetBarcode ) ];
                return Temp;
            }
        }

        public string Account
        {
            get { return Get( () => Account ); }
            set { Set( () => Account, value ); }
        }

        public string UserName
        {
            get { return Get( () => UserName ); }
            set { Set( () => UserName, value ); }
        }

        public string Password
        {
            get { return Get( () => Password ); }
            set { Set( () => Password, value ); }
        }

        public bool TestingServer
        {
            get { return Get( () => TestingServer ); }
            set { Set( () => TestingServer, value ); }
        }

        public bool MainServer
        {
            get { return Get( () => MainServer, true ); }
            set { Set( () => MainServer, value ); }
        }

        public bool SignInEnabled
        {
            get { return Get( () => SignInEnabled, false ); }
            set { Set( () => SignInEnabled, value ); }
        }

        private bool InServerChange;

        [ DependsUpon( nameof( Account ) ) ]
        [ DependsUpon( nameof( UserName ) ) ]
        [ DependsUpon( nameof( Password ) ) ]
        public void EnableSignIn()
        {
            SignInEnabled = Globals.OnLine && !string.IsNullOrEmpty( Account ) && !string.IsNullOrEmpty( UserName ) && !string.IsNullOrEmpty( Password );
        }

        [ DependsUpon( nameof( TestingServer ), Delay = 10 ) ]
        public void WhenTestingServerChanges()
        {
            if( !InServerChange )
            {
                InServerChange = true;
                MainServer = !TestingServer;
                InServerChange = false;
            }
        }

        [ DependsUpon( nameof( MainServer ) ) ]
        public void WhenMainServerChanges()
        {
            if( !InServerChange )
            {
                InServerChange = true;
                TestingServer = !MainServer;
                InServerChange = false;
            }
        }
    }
}