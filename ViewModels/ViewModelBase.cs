using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Timers;
using System.Windows.Input;
using Xamarin.Forms;

namespace Models.ViewModelSupport
{
    public class ViewModelBase : DynamicObject, INotifyPropertyChanged
    {
        protected readonly Dictionary<string, ICommand> Commands = new Dictionary<string, ICommand>();
        private readonly Dictionary<string, object> Values = new Dictionary<string, object>();
        private readonly IDictionary<string, List<string>> PropertyMap;
        private readonly IDictionary<string, List<string>> MethodMap;
        private readonly IDictionary<string, List<string>> CommandMap;
        private readonly IDictionary<string, int> DelayMap;
        private readonly Dictionary<string, Timer> ActiveDelayMap;

        public void Dispatcher( Action a )
        {
            Device.BeginInvokeOnMainThread( a );
        }

        [ AttributeUsage( AttributeTargets.Property | AttributeTargets.Method, AllowMultiple = true ) ]
        protected class DependsUponAttribute : Attribute
        {
            public string DependencyName { get; }
            public int Delay { get; set; }

            public bool VerifyStaticExistence { get; set; }

            public DependsUponAttribute( string propertyName )
            {
                DependencyName = propertyName;
            }
        }

        public static bool IsInDesignMode { get; }

        static ViewModelBase()
        {
            IsInDesignMode = false;
        }

        private IEnumerable<MemberInfo> _Methods;

        private IEnumerable<MemberInfo> GetMethods()
        {
            return _Methods ?? ( _Methods = GetType()
                                            .GetMethods()
                                            .Cast<MemberInfo>()
                                            .Where( method => !method.Name.StartsWith( CAN_EXECUTE_PREFIX ) ) );
        }

        public ViewModelBase()
        {
            PropertyMap = MapDependencies<DependsUponAttribute>( () => GetType().GetProperties() );
            MethodMap = MapDependencies<DependsUponAttribute>( GetMethods );

            CommandMap = MapDependencies<DependsUponAttribute>( () => GetType()
                                                                      .GetMethods()
                                                                      .Cast<MemberInfo>()
                                                                      .Where( method => method.Name.StartsWith( CAN_EXECUTE_PREFIX ) ) );
            DelayMap = MapDelays<DependsUponAttribute>( GetMethods );
            ActiveDelayMap = new Dictionary<string, Timer>();

            CreateCommands();
            VerifyDependancies();
        }

        protected T Get<T>( string name )
        {
            return Get( name, default(T) );
        }

        protected T Get<T>( string name, T defaultValue )
        {
            if( Values.ContainsKey( name ) )
                return (T)Values[ name ];

            return defaultValue;
        }

        protected T Get<T>( string name, Func<T> initialValue )
        {
            if( Values.ContainsKey( name ) )
                return (T)Values[ name ];

            Set( name, initialValue() );
            return Get<T>( name );
        }

        protected T Get<T>( Expression<Func<T>> expression )
        {
            return Get<T>( PropertyName( expression ) );
        }

        protected T Get<T>( Expression<Func<T>> expression, T defaultValue )
        {
            return Get( PropertyName( expression ), defaultValue );
        }

        protected T Get<T>( Expression<Func<T>> expression, Func<T> initialValue )
        {
            return Get( PropertyName( expression ), initialValue );
        }

        public void Set<T>( string name, T value )
        {
            if( Values.ContainsKey( name ) )
            {
                if( ( Values[ name ] == null ) && ( value == null ) )
                    return;

                if( ( Values[ name ] != null ) && Values[ name ].Equals( value ) )
                    return;

                Values[ name ] = value;
            }
            else
                Values.Add( name, value );

            RaisePropertyChanged( name );
        }

        protected void RaisePropertyChanged( string name )
        {
            PropertyChanged.Raise( this, name );

            if( PropertyMap.ContainsKey( name ) )
                PropertyMap[ name ].Each( RaisePropertyChanged );

            ExecuteDependentMethods( name );
            FireChangesOnDependentCommands( name );
        }

        private void ExecuteDependentMethods( string name )
        {
            if( MethodMap.ContainsKey( name ) )
                MethodMap[ name ].Each( ExecuteMethod );
        }

        private void FireChangesOnDependentCommands( string name )
        {
            if( CommandMap.ContainsKey( name ) )
                CommandMap[ name ].Each( RaiseCanExecuteChangedEvent );
        }

        protected void Set<T>( Expression<Func<T>> expression, T value )
        {
            Set( PropertyName( expression ), value );
        }

        private static string PropertyName<T>( Expression<Func<T>> expression )
        {
            if( !( expression.Body is MemberExpression MemberExpression ) )
                throw new ArgumentException( "expression must be a property expression" );

            return MemberExpression.Member.Name;
        }

        public override bool TryGetMember( GetMemberBinder binder, out object result )
        {
            result = Get<object>( binder.Name );

            return ( result != null ) || base.TryGetMember( binder, out result );
        }

        public override bool TrySetMember( SetMemberBinder binder, object value )
        {
            var Result = base.TrySetMember( binder, value );
            if( Result )
                return true;

            Set( binder.Name, value );
            return true;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private const string EXECUTE_PREFIX = "Execute_";
        private const string CAN_EXECUTE_PREFIX = "CanExecute_";

        private void CreateCommands()
        {
            var CNames = from Method in GetType().GetMethods()
                         where Method.Name.StartsWith( EXECUTE_PREFIX )
                         select Method.Name.StripLeft( EXECUTE_PREFIX.Length );

            foreach( var Name in CNames )
                Commands.Add( Name, new DelegateCommand<object>( x => ExecuteCommand( Name, x ), x => CanExecuteCommand( Name, x ) ) );
        }

        private void ExecuteCommand( string name, object parameter )
        {
            var MethodInfo = GetType().GetMethod( EXECUTE_PREFIX + name );
            if( MethodInfo == null ) return;

            MethodInfo.Invoke( this, MethodInfo.GetParameters().Length == 1 ? new[] { parameter } : null );
        }

        private bool CanExecuteCommand( string name, object parameter )
        {
            var MethodInfo = GetType().GetMethod( CAN_EXECUTE_PREFIX + name );
            if( MethodInfo == null ) return true;

            return (bool)MethodInfo.Invoke( this, MethodInfo.GetParameters().Length == 1 ? new[] { parameter } : null );
        }

        protected void RaiseCanExecuteChangedEvent( string canExecuteName )
        {
            var CommandName = canExecuteName.StripLeft( CAN_EXECUTE_PREFIX.Length );
            var Command = Get<DelegateCommand<object>>( CommandName );
            Command?.RaiseCanExecuteChanged();
        }

        private static IDictionary<string, List<string>> MapDependencies<T>( Func<IEnumerable<MemberInfo>> getInfo ) where T : DependsUponAttribute
        {
            var RetVal = new Dictionary<string, List<string>>();

            foreach( var Info in getInfo() )
            {
                var Name = Info.Name;
                var DepName = Info.GetCustomAttributes( typeof( T ), true ).Cast<T>().Select( a => a.DependencyName );
                foreach( var DName in DepName.Where( dName => !string.IsNullOrEmpty( dName ) ) )
                {
                    if( !RetVal.TryGetValue( DName, out var DepList ) )
                        RetVal.Add( DName, DepList = new List<string>() );

                    DepList.Add( Name );
                }
            }
            return RetVal;
        }

        private static IDictionary<string, int> MapDelays<T>( Func<IEnumerable<MemberInfo>> getInfo ) where T : DependsUponAttribute
        {
            var RetVal = new Dictionary<string, int>();
            foreach( var Info in getInfo() )
            {
                var Name = Info.Name;
                var Delay = Info.GetCustomAttributes( typeof( T ), true ).Cast<T>().Select( a => a.Delay ).FirstOrDefault();
                if( ( Delay > 0 ) && !RetVal.ContainsKey( Name ) )
                    RetVal.Add( Name, Delay );
            }
            return RetVal;
        }

        private void ExecuteMethod( string name )
        {
            var MemberInfo = GetType().GetMethod( name );
            if( MemberInfo == null )
                return;

            lock( ActiveDelayMap )
            {
                if( DelayMap.TryGetValue( name, out var Delay ) && ( Delay > 0 ) )
                {
                    if( !ActiveDelayMap.TryGetValue( name, out var Timer ) )
                    {
                        Timer = new Timer( Delay ) { AutoReset = false };
                        ActiveDelayMap.Add( name, Timer );
                        Timer.Elapsed += ( sender, args ) =>
                        {
                            Timer.Stop();
                            lock( ActiveDelayMap )
                                ActiveDelayMap.Remove( name );
                            Timer.Dispose();

                            Dispatcher( () => { MemberInfo.Invoke( this, null ); } );
                        };
                    }
                    else
                        Timer.Stop();
                    Timer.Start();
                    return;
                }
            }
            MemberInfo.Invoke( this, null );
        }

        private void VerifyDependancies()
        {
            var Methods = GetType().GetMethods().Cast<MemberInfo>();
            var Properties = GetType().GetProperties();

            var PropertyNames = Methods.Union( Properties )
                                       .SelectMany( method => method.GetCustomAttributes( typeof( DependsUponAttribute ), true ).Cast<DependsUponAttribute>() )
                                       .Where( attribute => attribute.VerifyStaticExistence )
                                       .Select( attribute => attribute.DependencyName );

            PropertyNames.Each( VerifyDependancy );
        }

        private void VerifyDependancy( string propertyName )
        {
            var Property = GetType().GetProperty( propertyName );
            if( Property == null )
                throw new ArgumentException( "DependsUpon Property Does Not Exist: " + propertyName );
        }
    }
}