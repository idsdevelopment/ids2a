﻿using System;
using System.Windows.Input;

namespace Models.ViewModelSupport
{
    public class DelegateCommand<T> : ICommand
    {
        private readonly Action<T> _Execute;
        private readonly Predicate<T> _CanExecute;

        public DelegateCommand( Action<T> execute )
            : this( execute, x => true )
        {
        }

        public DelegateCommand( Action<T> execute, Predicate<T> canExecute )
        {
            _Execute = execute ?? throw new ArgumentNullException( nameof( execute ) );
            _CanExecute = canExecute ?? throw new ArgumentNullException( nameof( canExecute ) );
        }

        public void Execute( object parameter )
        {
            _Execute( (T)parameter );
        }

        public bool CanExecute( object parameter )
        {
            return _CanExecute( (T)parameter );
        }

        public event EventHandler CanExecuteChanged;

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged.Raise( this );
        }
    }

    public class DelegateCommand : DelegateCommand<object>
    {
        public DelegateCommand( Action execute )
            : base( execute != null ? x => execute() : (Action<object>)null )
        {
        }

        public DelegateCommand( Action execute, Func<bool> canExecute )
            : base( execute != null ? x => execute() : (Action<object>)null,
                    canExecute != null ? x => canExecute() : (Predicate<object>)null )
        {
        }
    }
}