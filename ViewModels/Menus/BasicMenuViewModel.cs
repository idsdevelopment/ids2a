﻿using System;
using Models.ViewModelSupport;
using ViewModels.Menus.Constants;

namespace ViewModels.Menus
{
    public class BasicMenuViewModel : ViewModelBase
    {
        private const string SELECT_ITEM = "BasicMenuSelectedItem";

        private int SelectedItemIndex = -1;

        public Func<int> OnIdexOfSelectedItem;
        public Action<Constants.PROGRAMS> RunProgram;

        public BasicMenuViewModel()
        {
            SelectedItem = Globals.GetStringSetting( SELECT_ITEM );
        }

        public object SelectedItem
        {
            get { return Get( () => SelectedItem ); }
            set { Set( () => SelectedItem, value ); }
        }

        [ DependsUpon( nameof( SelectedItem ), Delay = 250 ) ]
        public void WhenSelectedIndexChanges()
        {
            if( SelectedItem is string Item )
            {
                Globals.SaveStringSetting( SELECT_ITEM, Item );
                SelectedItemIndex = OnIdexOfSelectedItem?.Invoke() ?? -1;

                var Pgm = PROGRAMS.NONE;
                switch( SelectedItemIndex )
                {
                case 0:
                    Pgm = PROGRAMS.TRIPS_BASIC;
                    break;
                }
                if( Pgm != PROGRAMS.NONE)
                    RunProgram?.Invoke( Pgm );
            }
        }
    }
}