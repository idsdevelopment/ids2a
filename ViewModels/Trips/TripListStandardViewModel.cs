﻿using System;
using System.Collections.Generic;
using System.Text;
using CommsDb.Interfaces;
using Interfaces.Interfaces;
using Models.ViewModelSupport;
using Service.Interfaces;

namespace ViewModels.Trips
{
    public class TripListStandardViewModel : ViewModelBase
    {
        public STATUS Status
        {
            get { return Get( () => Status ); }
            set { Set( () => Status, value ); }
        }
    }
}