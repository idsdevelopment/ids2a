﻿using System;
using CommsDb.Database;
using Service.Interfaces;

namespace PushDb
{
    public class PushDb<TTrip> : Db<TTrip> where TTrip : ITrip
    {
        public PushDb( IService service, string dbName ) : base( service, dbName )
        {
        }
    }
}
