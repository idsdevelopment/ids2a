﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Timers;
using Service.Interfaces;

namespace Service.Abstracts
{
    public abstract class AIService<TService> : IService
    {
        public TService Client { get; }

        private Action<PING_STATUS> _OnPing;

        public Action<PING_STATUS> OnPing
        {
            get => _OnPing;
            set
            {
                _OnPing = value;
                Task.Run( () =>
                {
                    PingPimerOnElapsed(this, null); // Initial ping kick off polling
                });
            }
        }

        public abstract Task<PING_STATUS> Ping();

        public abstract Task<CONNECTION_STATUS> Connect( string carrierId, string userName, string password );

        public abstract Task<(IList<ITrip> Trips, bool Ok)> GetTrips();

        public abstract Task<bool> UpdateTrip( ITrip trip );
        public abstract Task<bool> UpdateGps( IList<GpsPoint> points );

        private Timer PingPimer;

        protected AIService( TService client )
        {
            Client = client;
        }

        public static implicit operator TService( AIService<TService> s )
        {
            return s.Client;
        }


        public void EnablePing( int pingIntervalInSeconds )
        {
            if( PingPimer != null )
            {
                PingPimer.Stop();
                PingPimer.Dispose();
                PingPimer = null;
            }
            if( pingIntervalInSeconds > 0 )
            {
                PingPimer = new Timer( pingIntervalInSeconds * 1000 );
                PingPimer.Elapsed += PingPimerOnElapsed;
            }
        }

        private async void PingPimerOnElapsed( object sender, ElapsedEventArgs elapsedEventArgs )
        {
            PingPimer.Stop();
            try
            {
                OnPing?.Invoke( await Ping() );
            }
            finally
            {
                PingPimer.Start();
            }
        }
    }
}