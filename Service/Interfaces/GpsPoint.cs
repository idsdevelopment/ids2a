﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Interfaces
{
    public class GpsPoint
    {
        public DateTimeOffset Time { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }

    public interface IGps
    {
        Task<bool> UpdateGps( IList<GpsPoint> points );
    }
}