﻿using System;

namespace Service.Interfaces
{
    public static class TripExtensions
    {
        public static bool EqualTo( this ITrip t1, ITrip t2 )
        {
            return ( t1 != null ) && ( t2 != null )
                                  && ( t1.Status == t2.Status )
                                  && ( t1.TripId == t2.TripId )
                                  && ( t1.PickupCompany == t2.PickupCompany )
                                  && ( t1.PickupCompany == t2.PickupCompany )
                                  && ( t1.DeliveryCompany == t2.DeliveryCompany )
                                  && ( t1.Reference == t2.Reference )
                                  && ( t1.PickupTime == t2.PickupTime )
                                  && ( t1.DeliveryTime == t2.DeliveryTime )
                                  && ( t1.Weight == t2.Weight )
                                  && ( t1.Pieces == t2.Pieces )
                                  && ( t1.ReceivedByDevice == t2.ReceivedByDevice )
                                  && ( t1.ReadByDriver == t2.ReadByDriver );
        }

        public static ITrip Copy( this ITrip t, ITrip t1 )
        {
            t.TripId = t1.TripId;
            t.Status = t1.Status;
            t.ReceivedByDevice = t1.ReceivedByDevice;
            t.ReadByDriver = t1.ReadByDriver;
            t.PickupCompany = t1.PickupCompany;
            t.DeliveryCompany = t1.DeliveryCompany;
            t.Reference = t1.Reference;
            t.PickupTime = t1.PickupTime;
            t.DeliveryTime = t1.DeliveryTime;
            t.Weight = t1.Weight;
            t.Pieces = t1.Pieces;
            t.OriginalDataAsJSON = t1.OriginalDataAsJSON;
            return t;
        }
    }

    public enum STATUS : byte
    {
        NEW,
        TO_PICK_UP,
        TO_DELIVER
    }

    public interface ITrip
    {
        object OtigiinalObject { get; set; }
        string OriginalDataAsJSON { get; set; }

        STATUS Status { get; set; }

        bool ReceivedByDevice { get; set; }
        bool ReadByDriver { get; set; }

        string TripId { get; set; }
        string PickupCompany { get; set; }
        string DeliveryCompany { get; set; }
        string Reference { get; set; }

        DateTimeOffset PickupTime { get; set; }
        DateTimeOffset DeliveryTime { get; set; }

        decimal Weight { get; set; }
        decimal Pieces { get; set; }
    }
}