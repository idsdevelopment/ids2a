﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Service.Interfaces
{
    public enum CONNECTION_STATUS
    {
        BAD,
        OK,
        TIME_ERROR
    }

    public enum PING_STATUS
    {
        ONLINE,
        OFFLINE
    }

    public interface IService : IGps
    {
        Action<PING_STATUS> OnPing { get; set; }
        Task<PING_STATUS> Ping();
        Task<CONNECTION_STATUS> Connect( string carrierId, string userName, string password );
        Task<(IList<ITrip> Trips, bool Ok)> GetTrips();
        Task<bool> UpdateTrip( ITrip trip );
    }
}