﻿using System;

namespace Utils
{
    public class Distance
    {
        public enum MEASUREMENT
        {
            KILOMETRES,
            MILES,
            NAUTICAL_MILES,
            METRES
        }

        public class Point
        {
            public double Latitude { get; set; }

            public double Longitude { get; set; }

            public override string ToString()
            {
                return Latitude + "," + Longitude;
            }

            public bool IsZero => ( Latitude == 0 ) && ( Longitude == 0 );
        }

        public class Points
        {
            private readonly Point From;
            private readonly Point To;

            public Points( Point from, Point to )
            {
                From = from;
                To = to;
            }

            public bool IsZero => From.IsZero && To.IsZero;

            public double DistanceTo( MEASUREMENT measurement = MEASUREMENT.KILOMETRES )
            {
                var Rlat1 = Math.PI * From.Latitude / 180;
                var Rlat2 = Math.PI * To.Latitude / 180;
                var Theta = From.Longitude - To.Longitude;
                var Rtheta = Math.PI * Theta / 180;
                var Dist = Math.Sin( Rlat1 ) * Math.Sin( Rlat2 ) + Math.Cos( Rlat1 ) * Math.Cos( Rlat2 ) * Math.Cos( Rtheta );
                Dist = Math.Acos( Dist );
                Dist = Dist * 180 / Math.PI;
                Dist = Dist * 60 * 1.1515;

                if( double.IsNaN( Dist ) )
                    Dist = 0;

                switch( measurement )
                {
                case MEASUREMENT.KILOMETRES:
                    return Dist * 1.609344;
                case MEASUREMENT.METRES:
                    return Dist * 1.609344 * 1000;
                case MEASUREMENT.NAUTICAL_MILES:
                    return Dist * 0.8684;
                default: //Miles
                    return Dist;
                }
            }
        }

        public class DateTimePoint : Point
        {
            public DateTimeOffset DateTime;
        }

        public static bool IsDistanceOk( Point from, Point to, int minDistance )
        {
            var Dist = Math.Abs( new Points( from, to ).DistanceTo( MEASUREMENT.METRES ) );
            return Dist >= minDistance;
        }

        public static bool IsDistanceOk( double fromLat, double fromLong, double toLat, double toLong, int minDistance )
        {
            return IsDistanceOk( new Point { Latitude = fromLat, Longitude = fromLong }, new Point { Latitude = toLat, Longitude = toLong }, minDistance );
        }

        public static bool IsDistanceOk( DateTimePoint from, DateTimePoint to, int minDistance, int maxTimeStationaryInMinutes )
        {
            return ( new TimeSpan( ( from.DateTime - to.DateTime ).Ticks ).TotalMinutes >= maxTimeStationaryInMinutes ) || IsDistanceOk( from, to, minDistance );
        }
    }
}