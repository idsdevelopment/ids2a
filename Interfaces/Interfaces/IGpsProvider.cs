﻿using System;

namespace Interfaces.Interfaces
{
    public class GpsPoint
    {
        public DateTimeOffset Time { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }

    public interface IGpsProvider
    {
        Action<GpsPoint> OnNewPosition { get; set; }
    }
}