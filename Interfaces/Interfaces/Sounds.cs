﻿using System.Threading.Tasks;

namespace Interfaces.Interfaces
{
    public interface ISoundProvider
    {
        Task<bool> PlaySoundAsync( string filename );
        Task<bool> KeyboardClick();
    }
}