﻿using System;
using Newtonsoft.Json;
using Service.Interfaces;
using SQLite;

#pragma warning disable 660,661

namespace CommsDb.Abstracts
{
    public static class ATripExtensios
    {
    }

    public abstract class AITrip<T> : ITrip
    {
        public string TripId { get; set; }
        public STATUS Status { get; set; }

        public bool ReceivedByDevice { get; set; }
        public bool ReadByDriver { get; set; }

        public string PickupCompany { get; set; }
        public string DeliveryCompany { get; set; }
        public string Reference { get; set; }
        public DateTimeOffset PickupTime { get; set; }
        public DateTimeOffset DeliveryTime { get; set; }
        public decimal Weight { get; set; }
        public decimal Pieces { get; set; }

        [ Ignore ]
        public object OtigiinalObject
        {
            get => JsonConvert.DeserializeObject<T>( OriginalDataAsJSON );
            set => OriginalDataAsJSON = JsonConvert.SerializeObject( value );
        }

        public string OriginalDataAsJSON { get; set; }
    }
}