﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using CommsDb.Interfaces;
using Service.Interfaces;
using SQLite;

namespace CommsDb.Database
{
    // ReSharper disable once UnusedTypeParameter
    public partial class Db<TTrip> : SQLiteConnection, IDb where TTrip : ITrip
    {
        private const int DB_VERSION = 1;

        public class DbVersion
        {
            public int Version { get; set; } = DB_VERSION;
        }

        public readonly object LockObject = new object();

        private volatile bool DatabaseLock;

        private readonly IService Service;

        private static string DbPath( string dbName )
        {
            var Folder = Environment.GetFolderPath( Environment.SpecialFolder.Personal );
            return Path.Combine( Folder, dbName );
        }

        private void Opentable<T>( bool reCreate )
        {
            try
            {
                if( reCreate )
                {
                    try
                    {
                        DropTable<T>();
                    }
                    catch
                    {
                    }
                }
                CreateTable<T>();
            }
            catch // Probably restructured table
            {
                DropTable<T>();
                CreateTable<T>();
            }
        }

        private Polling ServicePolling;

        public int _PollingIntervalInSeconds;

        public int PollingIntervalInSeconds
        {
            get => _PollingIntervalInSeconds;
            set
            {
                ServicePolling?.Dispose();
                _PollingIntervalInSeconds = value;
                ServicePolling = new Polling( this, value );
            }
        }

        public Db( IService service, string dbName ) : base( DbPath( dbName ) )
        {
            Service = service;

            var ReCreate = true;

            CreateTable<DbVersion>();

            var Version = ( from V in Table<DbVersion>()
                            select V ).FirstOrDefault();

            if( Version != null )
            {
                ReCreate = Version.Version != DB_VERSION;
                if( ReCreate )
                    Opentable<DbVersion>( true );
            }
            Opentable<InTripFifo>( ReCreate );
            Opentable<OutTripFifo>( ReCreate );
            Opentable<Trip>( ReCreate );
            Opentable<Gps>( ReCreate );

            GpsUpdateTimer = new Timer( async state => { await UpdateGps(); }, null, GPS_UPDATE_TIME_LIMIT_IN_MINUTES * 60 * 1000, GPS_UPDATE_TIME_LIMIT_IN_MINUTES * 60 * 1000 );
        }

        private void ResetDatabaseLock()
        {
            lock( LockObject )
                DatabaseLock = false;
        }

        // Maintain processing order
#if DEBUG
        private string PrevCaller, Caller;

        private void SetDatabaseLock( [ CallerMemberName ] string callerName = "" )
        {
            if( DatabaseLock )
                Console.WriteLine( $"Locked by: {Caller}, Waiting for lock:{callerName}" );
            else
            {
                PrevCaller = Caller;
                Caller = callerName;
                Console.WriteLine( $"{PrevCaller}, {Caller}" );
            }
#else
        private void SetDatabaseLock()
        {
#endif
            while( true )
            {
                if( !DatabaseLock )
                {
                    lock( LockObject )
                    {
                        if( !DatabaseLock )
                        {
                            DatabaseLock = true;
                            return;
                        }
                    }
                }
                Thread.Sleep( 10 );
            }
        }
    }
}