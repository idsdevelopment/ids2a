﻿using System;
using System.Timers;
using SQLite;

namespace CommsDb.Database
{
    public partial class Db<TTrip> : SQLiteConnection
    {
        internal class Polling : IDisposable
        {
            private Timer ReceiveTimer;

            internal Polling( Db<TTrip> owner, int pollingIntervalInSeconds )
            {
                ReceiveTimer = new Timer( pollingIntervalInSeconds * 1000 );
                ReceiveTimer.Elapsed += async ( sender, args ) =>
                {
                    ReceiveTimer.Stop();
                    try
                    {
                        var (Trips, Ok) = await owner.Service.GetTrips();
                        if( Ok )
                            owner.PushIn( Trips );

                        owner.NotifyInFifoUpdate(); // Keep things ticking over
                        owner.NotifyOutFifoUpdate();
                    }
                    finally
                    {
                        ReceiveTimer.Start();
                    }
                };
            }

            public void Dispose()
            {
                if( ReceiveTimer != null )
                {
                    ReceiveTimer.Stop();
                    ReceiveTimer?.Dispose();
                    ReceiveTimer = null;

                }
            }
        }
    }
}