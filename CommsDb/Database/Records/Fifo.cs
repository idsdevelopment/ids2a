﻿using Service.Interfaces;
using SQLite;

namespace CommsDb.Database
{
    public partial class Db<TTrip>
    {
        internal class TripFifo : Trip
        {
            [ PrimaryKey ]
            [ AutoIncrement ]
            public int Id { get; set; }

            internal TripFifo()
            {
            }

            internal TripFifo( ITrip t )
            {
                this.Copy( t );
            }
        }

        internal class InTripFifo : TripFifo
        {
            public enum OPERATION
            {
                ADD,
                DELETE,
                REPLACE
            }

            public OPERATION Operation { get; set; }

            public InTripFifo()
            {
            }

            public InTripFifo( ITrip t ) : base( t )
            {
            }
        }

        internal class OutTripFifo : TripFifo
        {
            public OutTripFifo()
            {
            }

            public OutTripFifo( ITrip t ) : base( t )
            {
            }
        }

        internal InTripFifo InFofo
        {
            get
            {
                SetDatabaseLock();
                try
                {
                    var Rec = ( from I in Table<InTripFifo>()
                                orderby I.Id
                                select I ).FirstOrDefault();
                    if( Rec != null )
                        Delete( Rec );
                    return Rec;
                }
                finally
                {
                    ResetDatabaseLock();
                }
            }

            set
            {
                SetDatabaseLock();
                try
                {
                    Insert( value );
                }
                finally
                {
                    ResetDatabaseLock();
                    NotifyInFifoUpdate();
                }
            }
        }

        internal OutTripFifo OutFifo
        {
            get
            {
                SetDatabaseLock();
                try
                {
                    var Rec = ( from I in Table<OutTripFifo>()
                                orderby I.Id
                                select I ).FirstOrDefault();
                    if( Rec != null )
                        Delete( Rec );
                    return Rec;
                }
                finally
                {
                    ResetDatabaseLock();
                    NotifyOutFifoUpdate();
                }
            }

            set
            {
                SetDatabaseLock();
                try
                {
                    Insert( value );
                }
                finally
                {
                    ResetDatabaseLock();
                }
            }
        }
    }
}