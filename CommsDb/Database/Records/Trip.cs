﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommsDb.Abstracts;
using Service.Interfaces;

namespace CommsDb.Database
{
    public partial class Db<TTrip>
    {
        public class Trip : AITrip<TTrip>
        {
        }

        private bool InInNotify;

        internal void NotifyInFifoUpdate()
        {
            if( !InInNotify )
            {
                InInNotify = true;

                Task.Run( () =>
                {
                    SetDatabaseLock();
                    try
                    {
                        var Recs = ( from I in Table<InTripFifo>()
                                     orderby I.Id
                                     select I ).ToList();
                        foreach( var Rec in Recs )
                        {
                            var TRec = (Trip)Rec;
                            switch( Rec.Operation )
                            {
                            case InTripFifo.OPERATION.ADD:
                                TRec.ReceivedByDevice = true;
                                Insert( TRec );
                                OutFifo = new OutTripFifo( TRec );
                                break;

                            case InTripFifo.OPERATION.DELETE:
                                Delete( TRec );
                                break;

                            case InTripFifo.OPERATION.REPLACE:
                                var DbRec = ( from Dbr in Table<Trip>()
                                              where Dbr.TripId == Rec.TripId
                                              select Dbr ).FirstOrDefault();
                                if( DbRec != null )
                                {
                                    DbRec.Copy( Rec );
                                    Update( Rec );
                                }
                                break;
                            }
                        }
                    }
                    finally
                    {
                        ResetDatabaseLock();
                        InInNotify = false;
                    }
                } );
            }
        }

        private bool InOutNotify;

        internal void NotifyOutFifoUpdate()
        {
            if( !InOutNotify )
            {
                InOutNotify = true;
                Task.Run( async () =>
                {
                    SetDatabaseLock();
                    try
                    {
                        var Recs = ( from I in Table<OutTripFifo>()
                                     orderby I.Id
                                     select I ).ToList();
                        foreach( var Rec in Recs )
                        {
                            if( await Service.UpdateTrip( Rec ) )
                                Delete( Rec );
                            else
                                return; // Start Again Later
                        }
                    }
                    finally
                    {
                        ResetDatabaseLock();
                        InOutNotify = false;
                    }
                } );
            }
        }

        public ITrip GeTrip( string tripId )
        {
            SetDatabaseLock();
            try
            {
                return ( from Trip in Table<Trip>()
                         where Trip.TripId == tripId
                         select Trip ).FirstOrDefault();
            }
            finally
            {
                ResetDatabaseLock();
            }
        }

        public IList<ITrip> GeTrips()
        {
            SetDatabaseLock();
            try
            {
                return ( from Trip in Table<Trip>()
                         select (ITrip)Trip ).ToList();
            }
            finally
            {
                ResetDatabaseLock();
            }
        }

        public void PushIn( IList<ITrip> trips )
        {
            var DbTrips = GeTrips();

            // Do Deletes First
            var DeletedTrips = DbTrips.Where( dbTrip => !trips.Any( dbTrip.EqualTo ) ).ToList();

            foreach( var DeletedTrip in DeletedTrips )
            {
                InFofo = new InTripFifo( DeletedTrip )
                         {
                             Operation = InTripFifo.OPERATION.DELETE
                         };
            }

            var NewTrips = trips.Where( trip => !DbTrips.Any( trip.EqualTo ) ).ToList();
            foreach( var NewTrip in NewTrips )
            {
                InFofo = new InTripFifo( NewTrip )
                         {
                             Operation = InTripFifo.OPERATION.ADD
                         };
            }

            var ModifiedTrips = trips.Where( trip => DbTrips.Where( dbTrip => trip.TripId == dbTrip.TripId ).Any( dbTrip => !dbTrip.EqualTo( trip ) ) ).ToList();
            foreach( var ModifiedTrip in ModifiedTrips )
            {
                InFofo = new InTripFifo( ModifiedTrip )
                         {
                             Operation = InTripFifo.OPERATION.REPLACE
                         };
            }

            NotifyInFifoUpdate();
        }

        public void Update( ITrip trip )
        {
            SetDatabaseLock();
            try
            {
                var Rec = ( from T in Table<Trip>()
                            where T.TripId == trip.TripId
                            select T ).FirstOrDefault();
                if( Rec != null )
                {
                    Rec.Copy( trip );
                    Update( Rec );
                    OutFifo = new OutTripFifo( trip );
                }
            }
            finally
            {
                ResetDatabaseLock();
            }
        }
    }
}