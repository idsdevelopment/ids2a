﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Service.Interfaces;
using SQLite;

namespace CommsDb.Database
{
    // ReSharper disable once UnusedTypeParameter
    public partial class Db<TTrip>
    {
        public const int GPS_UPDATE_TIME_LIMIT_IN_MINUTES = 10;

        public class Gps
        {
            [ PrimaryKey ]
            [ AutoIncrement ]
            public int Id { get; set; }

            public DateTimeOffset DateTime { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
        }

        // ReSharper disable once StaticMemberInGenericType
        private static readonly long UpdateTicks = new TimeSpan( 0, GPS_UPDATE_TIME_LIMIT_IN_MINUTES, 0 ).Ticks;

        // ReSharper disable once NotAccessedField.Local
        private Timer GpsUpdateTimer;

        private bool InUpdateGps;

        private async Task UpdateGps()
        {
            if( !InUpdateGps )
            {
                InUpdateGps = true;
                try
                {
                    SetDatabaseLock();
                    var Recs = ( from G in Table<Gps>()
                                 select G ).ToList();
                    ResetDatabaseLock();

                    if( Recs.Count < 100 )
                    {
                        var Ticks = DateTime.UtcNow.AddMinutes( GPS_UPDATE_TIME_LIMIT_IN_MINUTES ).Ticks;

                        var Rec = ( from R in Recs
                                    where Ticks - R.DateTime.Ticks > UpdateTicks
                                    select R ).FirstOrDefault();
                        if( Rec == null )
                            return;
                    }

                    if( await Service.UpdateGps( ( from R in Recs select new GpsPoint { Longitude = R.Longitude, Latitude = R.Latitude, Time = R.DateTime } ).ToList() ) )
                    {
                        SetDatabaseLock();
                        foreach( var Rec in Recs )
                            Delete( Rec );
                        ResetDatabaseLock();
                    }
                }
                finally
                {
                    InUpdateGps = false;
                }
            }
        }

        public async void AddGpsPoint( DateTimeOffset date, double latitude, double longitude )
        {
            SetDatabaseLock();
            Insert( new Gps { DateTime = date, Latitude = latitude, Longitude = longitude } );
            ResetDatabaseLock();
            await UpdateGps();
        }
    }
}