﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Service.Interfaces;

namespace CommsDb.Communications
{
    internal abstract class AIService : IService
    {
        public Action<PING_STATUS> OnPing { get; set; }

        public abstract Task<PING_STATUS> Ping();
        public abstract Task<CONNECTION_STATUS> Connect( string carrierId, string userName, string password );

        public abstract Task<(IList<ITrip> Trips, bool Ok)> GetTrips();
        public abstract Task<bool> UpdateTrip( ITrip trip );
        public Task<bool> UpdateGps( IList<GpsPoint> points )
        {
            throw new NotImplementedException();
        }
    }
}