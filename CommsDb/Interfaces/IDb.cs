﻿using System.Collections.Generic;
using Service.Interfaces;

namespace CommsDb.Interfaces
{
    public interface IDb
    {
        ITrip GeTrip(string tripId);
        IList<ITrip> GeTrips();
        void Update( ITrip trip);
    }
}