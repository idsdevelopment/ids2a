﻿using System.Threading;
using System.Threading.Tasks;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

namespace Ids2A.Droid
{
    [ Activity( Label = "Ids2A",
        Icon = "@drawable/icon",
        Theme = "@style/SplashScreen",
        MainLauncher = true,
        AlwaysRetainTaskState = true,
        ClearTaskOnLaunch = true,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.Keyboard,
        ScreenOrientation = ScreenOrientation.Portrait ) ]
    public class MainActivity : FormsAppCompatActivity
    {
        public static MainActivity Instance { get; private set; }

        protected override async void OnCreate( Bundle bundle )
        {
            Instance = this;
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate( bundle );

            // Now must explicitly request permissions
            if ( Globals.Android.MajorVersion >= 6 )
            {

                await AskPermission( new[] { Manifest.Permission.AccessFineLocation, Manifest.Permission.AccessCoarseLocation }, PERMISSIONS.LOCATION_PERMISSION_REQUEST );
                if( IsFinishing )
                    return;

                await AskPermission( new[] { Manifest.Permission.WriteExternalStorage, Manifest.Permission.ReadExternalStorage }, PERMISSIONS.STORAGE_REQUEST );
                if( IsFinishing )
                    return;

                if( Globals.Android.MajorVersion >= 7 )
                {
                    await AskPermission( new[] { Manifest.Permission.RequestInstallPackages, Manifest.Permission.RequestInstallPackages }, PERMISSIONS.INSTALL_REQUEST );
                    if( IsFinishing )
                        return;

                    await AskPermission( new[] { Manifest.Permission.RequestIgnoreBatteryOptimizations, Manifest.Permission.RequestIgnoreBatteryOptimizations }, PERMISSIONS.NO_DOZE );
                    if( IsFinishing )
                        return;
                }
            }

            Forms.Init( this, bundle );
            var App = new App();

            var InT = new Intent( this, typeof( PeriodicService ) );
            StartService( InT );

            LoadApplication( App );
        }

        private enum PERMISSIONS
        {
            LOCATION_PERMISSION_REQUEST,
            STORAGE_REQUEST,
            INSTALL_REQUEST,
            NO_DOZE
        }

        public override void OnRequestPermissionsResult( int requestCode, string[] permissions, Permission[] grantResults )
        {
            if( ( grantResults.Length <= 0 ) || ( grantResults[ 0 ] == Permission.Denied ) )
            {
                string Text;

                switch( (PERMISSIONS)requestCode )
                {
                case PERMISSIONS.LOCATION_PERMISSION_REQUEST:
                    Text = "GPS";
                    break;
                case PERMISSIONS.STORAGE_REQUEST:
                    Text = "Stroage";
                    break;
                case PERMISSIONS.INSTALL_REQUEST:
                    Text = "Install";
                    break;

                case PERMISSIONS.NO_DOZE:
                    Text = "Disable Battery Optimisation";
                    break;

                default:
                    return;
                }

                var Alert = new AlertDialog.Builder( this );
                Alert.SetTitle( "Cannot continue" );
                Alert.SetMessage( $"Unable to continue without {Text} permission." );
                Alert.SetPositiveButton( "Ok", ( sender, args ) => { Finish(); } );

                var Dialog = Alert.Create();
                Dialog.Show();
            }
        }

        private async Task AskPermission( string[] permissions, PERMISSIONS requestCode )
        {
            await Task.Run( () =>
            {
                var AskingPermission = false;
                var PrimaryPermission = permissions[ 0 ];

                // Here, thisActivity is the current activity
                while( ( ContextCompat.CheckSelfPermission( this, PrimaryPermission ) != Permission.Granted ) && !IsFinishing )
                {
                    if( !AskingPermission )
                    {
                        AskingPermission = true;

                        if( !ActivityCompat.ShouldShowRequestPermissionRationale( this, PrimaryPermission ) )
                            ActivityCompat.RequestPermissions( this, permissions, (int)requestCode );
                        else
                            break;
                    }
                    Thread.Sleep( 250 );
                }
            } );
        }
    }
}