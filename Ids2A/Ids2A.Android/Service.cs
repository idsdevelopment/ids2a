﻿using System;
using System.Timers;
using Android.App;
using Android.Content;
using Android.OS;
using Interfaces;

namespace Ids2A.Droid
{
    [ Service ]
    public class PeriodicService : Android.App.Service
    {
        private static PeriodicService _Service;
        private static readonly object LockObject = new object();

        public static PeriodicService Service
        {
            get
            {
                lock( LockObject )
                    return _Service;
            }
            set
            {
                lock( LockObject )
                    _Service = value;
            }
        }

        public const int PING_INTERVAL = 15000;

        public override IBinder OnBind( Intent intent )
        {
            return null;
        }

        public override StartCommandResult OnStartCommand( Intent intent, StartCommandFlags flags, int startId )
        {
            Service = this;
            return StartCommandResult.StickyCompatibility;
        }
    }
}