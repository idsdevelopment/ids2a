﻿using System;

namespace Ids2A.Droid
{
    public static class Globals
    {
        public static class Android
        {
            private static int _MajorVersion = -1, _MinorVersion = -1, _Build = -1, AsInt = -1;

            // ReSharper disable once MemberHidesStaticFromOuterClass
            public static string Version
            {
                get
                {
                    var Vsn = global::Android.OS.Build.VERSION.Release;
                    var Temp = Vsn.Split( new[] { '.' }, StringSplitOptions.RemoveEmptyEntries );
                    _MajorVersion = int.Parse( Temp[ 0 ] );
                    _MinorVersion = int.Parse( Temp[ 1 ] );
                    _Build = int.Parse( Temp[ 1 ] );

                    return Vsn;
                }
            }

            public static int MajorVersion
            {
                get
                {
                    if( _MajorVersion == -1 )
                    {
                        var V = Version;
                    }
                    return _MajorVersion;
                }
            }

            public static int MinorVersion
            {
                get
                {
                    if( _MinorVersion == -1 )
                    {
                        var V = Version;
                    }
                    return _MinorVersion;
                }
            }

            public static int Build
            {
                get
                {
                    if( _Build == -1 )
                    {
                        var V = Version;
                    }
                    return _Build;
                }
            }

            public static int VersionAsInt
            {
                get
                {
                    if( AsInt == -1 ) // Needs {} for scope
                        AsInt = int.Parse( Version.Replace( ".", "" ).Trim() );

                    return AsInt;
                }
            }

            public static int ConvertPixelsToDp( float pixelValue )
            {
                return (int)( pixelValue / MainActivity.Instance.Resources.DisplayMetrics.Density );
            }

            public static int ConvertDpToPixels( float pixelValue )
            {
                return (int)( pixelValue * MainActivity.Instance.Resources.DisplayMetrics.Density );
            }
        }
    }
}