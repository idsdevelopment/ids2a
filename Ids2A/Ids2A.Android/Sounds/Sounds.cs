﻿using System.Threading.Tasks;
using Android.Content;
using Android.Media;
using Ids2A.Droid;
using Interfaces.Interfaces;
using Xamarin.Forms;

[ assembly: Dependency( typeof( AndroidSoundProvider ) ) ]

namespace Ids2A.Droid
{
    public class AndroidSoundProvider : ISoundProvider
    {
        public Task<bool> PlaySoundAsync( string filename )
        {
            filename = $"Sounds/{filename}";

            // Create media player
            var Player = new MediaPlayer();

            // Create task completion source to support async/await
            var Tsk = new TaskCompletionSource<bool>();

            // Hook up some events
            Player.Prepared += ( s, e ) => { Player.Start(); };

            Player.Completion += ( sender, e ) =>
            {
                Player.Release();
                Tsk.SetResult( true );
            };

            // Open the resource
            var Fd = MainActivity.Instance.Assets.OpenFd( filename );
            Player.SetDataSource( Fd.FileDescriptor, Fd.StartOffset, Fd.Length );
            Fd.Close();

            Player.Prepare();

            return Tsk.Task;
        }

        public Task<bool> KeyboardClick()
        {
            var Tsk = new TaskCompletionSource<bool>();

            MainActivity.Instance.RunOnUiThread( () =>
            {
                var AudioManager = (AudioManager)MainActivity.Instance.GetSystemService( Context.AudioService );

                AudioManager.PlaySoundEffect( SoundEffect.KeyClick );

                Tsk.SetResult( true );
            } );
            return Tsk.Task;
        }
    }
}