﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android.Content;
using Android.Locations;
using Android.OS;
using Android.Provider;
using Ids2A.Droid.Gps;
using Interfaces.Interfaces;
using Xamarin.Forms;
using Object = Java.Lang.Object;

[ assembly: Dependency( typeof( AndroidGpsProviderProvider ) ) ]

namespace Ids2A.Droid.Gps
{
    public class AndroidGpsProviderProvider : Object, ILocationListener, IGpsProvider
    {
        private LocationManager Manager;
        private string Provider = "";

        private readonly Criteria CriteriaForLocationService = new Criteria
                                                               {
                                                                   Accuracy = Accuracy.Fine
                                                               };

        public Action<GpsPoint> OnNewPosition { get; set; }

        public AndroidGpsProviderProvider()
        {
            var Ctx = MainActivity.Instance;
            Manager = (LocationManager)Ctx.GetSystemService( Context.LocationService );

            GetProvider();

            // If GPS is off open up GPS settings
            if( string.IsNullOrEmpty( Provider ) || !Manager.IsProviderEnabled( Provider ) )
            {
                MainActivity.Instance.RunOnUiThread( () =>
                {
                    var GpsSettingsIntent = new Intent( Settings.ActionLocationSourceSettings );
                    GpsSettingsIntent.SetFlags( ActivityFlags.NewTask );
                    Ctx.StartActivity( GpsSettingsIntent );
                } );
            }
            WaitProvider();
        }

        private void GetProvider()
        {
            if( Globals.Android.VersionAsInt <= 412 )
                Provider = LocationManager.GpsProvider;
            else
            {
                var AcceptableLocationProviders = Manager.GetProviders( CriteriaForLocationService, true );
                Provider = AcceptableLocationProviders.Any() ? AcceptableLocationProviders.First() : string.Empty;
            }
        }

        private void WaitProvider()
        {
            Task.Run( () =>
            {
                while( true )
                {
                    GetProvider();

                    if( string.IsNullOrWhiteSpace( Provider ) || !Manager.IsProviderEnabled( Provider ) )
                        Thread.Sleep( 100 );
                    else
                        break;
                }
                MainActivity.Instance.RunOnUiThread( () => { Manager.RequestLocationUpdates( Provider, 1500, 1, this ); } );
            } );
        }

        protected override void Dispose( bool disposing )
        {
            if( disposing && ( Manager != null ) )
            {
                Manager.RemoveUpdates( this );
                Manager.Dispose();
                Manager = null;
            }
            base.Dispose( disposing );
        }

        // ILocationListener Interface routines
        public void OnLocationChanged( Location location )
        {
            OnNewPosition?.Invoke( new GpsPoint
                                    {
                                        Latitude = location.Latitude,
                                        Longitude = location.Longitude,
                                        Time = DateTimeOffset.FromUnixTimeMilliseconds( location.Time )
                                    } );
        }

        public void OnProviderDisabled( string provider )
        {
        }

        public void OnProviderEnabled( string provider )
        {
        }

        public void OnStatusChanged( string provider, Availability status, Bundle extras )
        {
        }
    }
}