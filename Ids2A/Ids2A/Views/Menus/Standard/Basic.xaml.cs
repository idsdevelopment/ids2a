﻿using System.Collections.Generic;
using Ids2A.Views.Standard;
using ViewModels.Menus;
using ViewModels.Menus.Constants;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ids2A.Views.Menus.Standard
{
    [ XamlCompilation( XamlCompilationOptions.Compile ) ]
    public partial class Basic : ContentView
    {
        public Basic()
        {
            InitializeComponent();
            BindingContext = new BasicMenuViewModel
                             {
                                 OnIdexOfSelectedItem = () =>
                                 {
                                     if( ListView.ItemsSource is string[] Items )
                                     {
                                         var SelItem = (string)ListView.SelectedItem;
                                         var Ndx = 0;
                                         foreach( var Item in Items )
                                         {
                                             if( Item == SelItem )
                                                 return Ndx;
                                             Ndx++;
                                         }
                                     }
                                     return -1;
                                 },

                                 RunProgram = program =>
                                 {
                                     switch( program )
                                     {
                                     case PROGRAMS.TRIPS_BASIC:
                                         Globals.LoadView( new TripView() );
                                         break;
                                     }
                                 }
                             };
        }
    }
}