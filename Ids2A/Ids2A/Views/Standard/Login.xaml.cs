﻿using Ids2A.Views.Menus.Standard;
using Utils;
using ViewModels.Login;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ids2A.Views.Standard
{
    [ XamlCompilation( XamlCompilationOptions.Compile ) ]
    public partial class Login : ContentView
    {
        public Login()
        {
            InitializeComponent();
            var Model = new LoginPageViewModel();
            Globals.MainPageViewModel.MainTitle = (string)Globals.GetResource( "MainTitle" );

            Model.OnTimeError = () => { Model.Dispatcher( () => { Globals.MainPage.DisplayAlert( "Time Error", "Please correct the time on the phone", "Ok" ); } ); };
            Model.OnSignIn = ok =>
            {
                if( !ok )
                    Globals.MainPage.DisplayAlert( "Sign in Error", "Incorrect sign in", "Ok" );
                else
                {
                    Globals.LoadView( new ContentView() );      // Reduce flicker
                    Globals.MainPageViewModel.MainTitle = Model.UserName.Capitalise();
                    Globals.BarcodeScannerVisible = true;
                    Globals.LoadMenu( new Basic() );
                }
            };
            BindingContext = Model;
        }
    }
}