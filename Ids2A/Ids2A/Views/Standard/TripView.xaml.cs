﻿using ViewModels.Trips;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ids2A.Views.Standard
{
    [ XamlCompilation( XamlCompilationOptions.Compile ) ]
    public partial class TripView : ContentView
    {
        public TripView()
        {
            InitializeComponent();
            BindingContext = new TripListStandardViewModel();
        }
    }
}