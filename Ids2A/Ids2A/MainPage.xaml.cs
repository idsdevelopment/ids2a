﻿using System;
using System.Threading.Tasks;
using Ids2A.Views.Standard;
using ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ids2A
{
    [ ContentProperty( "Source" ) ]
    public class ImageResourceExtension : IMarkupExtension
    {
        public string Source { get; set; }

        public object ProvideValue( IServiceProvider serviceProvider )
        {
            return Source == null ? null : ImageSource.FromResource( Source );
        }
    }

    public static class Loader
    {
        public static void LoadView( ContentView view )
        {
            MainPage.LoadView( view );
        }
    }

    public partial class MainPage : ContentPage
    {
        public Action OnBackButton;

        private readonly MainPageViewModel Model;

        private bool InBackButton;

        protected override bool OnBackButtonPressed()
        {
            if( !InBackButton )
            {
                InBackButton = true;
                try
                {
                    base.OnBackButtonPressed();
                    if( Model.MenuVisible )
                    {
                        Model.MenuVisible = false;
                    }
                    else
                        OnBackButton?.Invoke();
                }
                finally
                {
                    InBackButton = false;
                }
            }
            return true;
        }

        public MainPage()
        {
            Globals.MainPage = this;
            InitializeComponent();
            Model = new MainPageViewModel
                    {
                        OnShowMenu = async show =>
                        {
                            if( show )
                            {
                                SideMenu.AnchorX = 1;
                                SideMenu.AnchorY = 0;

                                MenuBackground.Opacity = 0;
                                MenuBackground.IsVisible = true;
                                BaseLayout.RaiseChild( MenuBackground );

                                await SideMenu.ScaleTo( 0, 0 );
                                await SideMenu.FadeTo( 0, 0 );

                                SideMenu.Opacity = 0;
                                SideMenu.IsVisible = true;
                                BaseLayout.RaiseChild( SideMenu );


                                await Task.WhenAny(
                                                   MenuBackground.FadeTo( 0.3, 500, Easing.Linear ),
                                                   SideMenu.FadeTo( 1, 500, Easing.CubicOut ),
                                                   SideMenu.RelScaleTo( 1, 500, Easing.CubicOut )
                                                  );
                            }
                            else
                            {
                                SideMenu.AnchorX = 1;
                                SideMenu.AnchorY = 0;


                                await Task.WhenAny(
                                                   MenuBackground.FadeTo( 0, 500, Easing.Linear ),
                                                   SideMenu.FadeTo( 0, 500, Easing.CubicIn ),
                                                   SideMenu.ScaleTo( 0, 500, Easing.CubicIn )
                                                  );

                                SideMenu.IsVisible = false;
                                MenuBackground.IsVisible = false;
                            }
                        },
                        OnSignOut = () => { Globals.LoadView( new Login() ); }
                    };
            BindingContext = Model;
            Globals.MainPageViewModel = Model;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MenuBackground.IsVisible = false;
            SideMenu.IsVisible = false;

            LoadView( new Login() );
        }

        public static void LoadView( ContentView view )
        {
            var M = Globals.MainPage;
            if( M != null )
            {
                var C = M.ViewArea.Children;
                C.Clear();
                C.Add( view );
            }
        }

        public static void LoadMenu( ContentView view )
        {
            var M = Globals.MainPage;
            if( M != null )
            {
                var C = M.MenuLoadArea.Children;
                C.Clear();
                C.Add( view );
            }
        }

        private void MenuImageTapGestureRecognizer_OnTapped( object sender, EventArgs e )
        {
            Globals.SoundService.KeyboardClick();
        }
    }
}