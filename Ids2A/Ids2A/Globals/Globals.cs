﻿using System;
using Interfaces.Interfaces;
using ViewModels;
using Xamarin.Forms;

namespace Ids2A
{
    public static class Globals
    {
        public static MainPage MainPage;
        public static MainPageViewModel MainPageViewModel;

        private static ISoundProvider _SoundService;

        public static ISoundProvider SoundService => _SoundService ?? ( _SoundService = DependencyService.Get<ISoundProvider>() );

        public static bool BarcodeScannerVisible
        {
            get => ( MainPageViewModel != null ) && MainPageViewModel.BarcodeScannerVisible;
            set
            {
                if( MainPageViewModel != null )
                    MainPageViewModel.BarcodeScannerVisible = value;
            }
        }

        public static Action<string> OnScannedBarcode
        {
            get => MainPageViewModel.OnScannedBarcode;
            set => MainPageViewModel.OnScannedBarcode = value;
        }

        public static bool MenuVisible
        {
            get => MainPageViewModel.MenuVisible;
            set => MainPageViewModel.MenuVisible = value;
        }

        public static Action OnBackButon
        {
            get => MainPage.OnBackButton;
            set => MainPage.OnBackButton = value;
        }

        public static void LoadView( ContentView view )
        {
            MainPage.LoadView( view );
        }

        public static void LoadMenu( ContentView view )
        {
            MainPage.LoadMenu( view );
        }

        public static object GetResource( string resourceName )
        {
            try
            {
                return Application.Current.Resources[ resourceName ];
            }
            catch
            {
                return null;
            }
        }
    }
}