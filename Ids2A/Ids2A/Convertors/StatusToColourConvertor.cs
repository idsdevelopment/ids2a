﻿using System;
using System.Globalization;
using CommsDb.Interfaces;
using Service.Interfaces;
using Xamarin.Forms;

namespace Ids2A.Convertors
{
    public class StatusToColourConvertor : IValueConverter
    {
        private static bool HaveColours;
        private static Color ToPickup;

        public object Convert( object value, Type targetType, object parameter, CultureInfo culture )
        {
            if( value is STATUS Status )
            {
                if( !HaveColours )
                {
                    HaveColours = true;
                    ToPickup = (Color)Globals.GetResource( "PickedUpColor" );
                }
                switch( Status )
                {
                case STATUS.TO_PICK_UP:
                    return ToPickup;
                }
            }
            return 0xffffff;
        }

        public object ConvertBack( object value, Type targetType, object parameter, CultureInfo culture )
        {
            throw new NotImplementedException();
        }
    }
}