﻿using System;
using Newtonsoft.Json;
using Service.Interfaces;

namespace AzureClient.Model
{
    public class Trip : Protocol.Data.Trip, ITrip
    {
        public object OtigiinalObject
        {
            get => JsonConvert.DeserializeObject<Protocol.Data.Trip>( OriginalDataAsJSON );
            set => OriginalDataAsJSON = JsonConvert.SerializeObject( value );
        }

        public string OriginalDataAsJSON { get; set; }
        public STATUS Status { get; set; }

        public bool ReceivedByDevice { get; set; }
        public bool ReadByDriver { get; set; }

        public string PickupCompany { get; set; }
        public string DeliveryCompany { get; set; }
        public string Reference { get; set; }
        public DateTimeOffset PickupTime { get; set; }
        public DateTimeOffset DeliveryTime { get; set; }
        public decimal Weight { get; set; }
        public decimal Pieces { get; set; }
    }
}