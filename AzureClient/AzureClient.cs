﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ids;
using Protocol.Data;
using Service.Abstracts;
using Service.Interfaces;
using Utils;
using GpsPoint = Service.Interfaces.GpsPoint;

namespace AzureRemoteService
{
    public class AzureClient : AIService<IdsClient>
    {
        public const int PING_INTERVAL_IN_SECONDS = 15;

        private const string PRODUCTION_END_POINT = "http://idsroute.azurewebsites.net/",
                             ALPHAT_END_POINT = "https://idsroute-alphat.azurewebsites.net",
                             ALPHAC_END_POINT = "https://idsroute-alphac.azurewebsites.net",
                             BETA_END_POINT = "https://idsroute-beta.azurewebsites.net",
                             GAMMA_END_POINT = "https://idsroute-gamma.azurewebsites.net";

        public const string TIME_ERROR = "TimeError";

        public static bool _DebugServer = false;

        public enum SLOT
        {
            PRODUCTION,
            ALPHAT,
            ALPHAC,
            BETA,
            GAMMA
        }

#if ALPHA_T
        public static SLOT Slot = SLOT.ALPHAT;
#else
        public static SLOT Slot = SLOT.PRODUCTION;
#endif

        private static readonly object LockObject = new object();
        public static string _AuthToken = "";
        public string DebugServer => _DebugServer ? "t" : "f";

        public static string AuthToken
        {
            set
            {
                if( string.IsNullOrEmpty( value ) )
                    Console.WriteLine( "Auth token set to empty" );
                lock( LockObject )
                    _AuthToken = value;
            }
            get
            {
                lock( LockObject )
                    return _AuthToken;
            }
        }

        public static string GetSlot()
        {
            switch( Slot )
            {
            case SLOT.ALPHAT:
                return ALPHAT_END_POINT;
            case SLOT.ALPHAC:
                return ALPHAC_END_POINT;
            case SLOT.BETA:
                return BETA_END_POINT;
            case SLOT.GAMMA:
                return GAMMA_END_POINT;
            default:
                return PRODUCTION_END_POINT;
            }
        }

        public AzureClient() : base( new IdsClient( GetSlot(), AuthToken ) )
        {
            EnablePing( PING_INTERVAL_IN_SECONDS );
        }

        public override async Task<PING_STATUS> Ping()
        {
            try
            {
                var Response = await Client.RequestPing();
                return Response == "OK" ? PING_STATUS.ONLINE : PING_STATUS.OFFLINE;
            }
            catch( Exception E )
            {
                Console.WriteLine( E );
            }
            return PING_STATUS.OFFLINE;
        }

        public override async Task<CONNECTION_STATUS> Connect( string carrierId, string userName, string password )
        {
            AuthToken = await Client.RequestLoginAsDriver( Encryption.ToTimeLimitedToken( carrierId.Trim() ), Encryption.ToTimeLimitedToken( userName.Trim() ),
                                                           Encryption.ToTimeLimitedToken( password ), DateTimeOffset.Now.Offset.Hours.ToString(),
                                                           "Ids2A",
                                                           DebugServer, Encryption.ToTimeLimitedToken( DateTime.UtcNow.Ticks.ToString() ) );

            if( !string.IsNullOrWhiteSpace( AuthToken ) )
                return AuthToken != TIME_ERROR ? CONNECTION_STATUS.OK : CONNECTION_STATUS.TIME_ERROR;

            return CONNECTION_STATUS.BAD;
        }

        public override Task<(IList<ITrip> Trips, bool Ok)> GetTrips()
        {
            throw new NotImplementedException();
        }

        public override Task<bool> UpdateTrip( ITrip trip )
        {
            throw new NotImplementedException();
        }

        public bool UpdateTrip( Trip trip )
        {
            throw new NotImplementedException();
        }

        public override async Task<bool> UpdateGps( IList<GpsPoint> points )
        {
            try
            {
                var Points = new GpsPoints();
                Points.AddRange( points.Select( point => new Protocol.Data.GpsPoint { Longitude = point.Longitude, Latitude = point.Latitude, LocalDateTime = point.Time } ) );
                await Client.RequestGps( Points );
                return true;
            }
            catch
            {
            }
            return false;
        }
    }
}